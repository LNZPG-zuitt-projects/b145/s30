console.log('Hello');
console.log('Love');
console.log('Goodbye');


// for(let i = 0; i <= 1500; i++){
// 	console.log(i)
// };

console.log('loop is done');

// Getting all posts

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));


// checking the status of request
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));


// Async and Await

async function fetchData(){

	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	let json = await result.json();
	console.log(json);

}

fetchData();

// Get specific post
	// (/post/:id)

fetch('https://jsonplaceholder.typicode.com/posts/64')
	.then((response) => response.json())
	.then((json) => console.log(json))


	// Create a post

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		header: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'New Post',
			body : 'I am a new post',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	// Updating a post
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'PUT',
		header: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated Post',
			body: 'This is an updated post',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


	// updating post using PATCH
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'Correct post'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


	// Delete a post
	fetch('https://jsonplaceholder.typicode.com/posts/12', {
		method: 'DELETE',
	})


	// Filtering Posts
	fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=10')
	.then((response) => response.json())
	.then((json) => console.log(json));

	// Retrieveng specific comments
	fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((json) => console.log(json));