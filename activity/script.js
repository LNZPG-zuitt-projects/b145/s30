// console.log('Itlog');

// 4.0
fetch('https://jsonplaceholder.typicode.com/todos');
	.then((response) => response.json())
	.then((json) => console.log(json))

	let list = json.map((todo => {
		return todo.title;
	}))

// 6.0
fetch('https://jsonplaceholder.typicode.com/todos/1');
	.then((response) => response.json())
	.then((json) => console.log(`the item "${json.title}`))


// 7.0
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers:  {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Task',
		body: 'This is a new task',
		userId: 1
	})
})
	.then((response) => response.json())
	.then((json) => console.log(json))

// 8.0 - 9.0
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers:  {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		body: 'To update the my to do list with a different data structure',
		userId: 1
	})
});
	.then((response) => response.json())
	.then((json) => console.log(json))


// 10.0 - 11.0
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers:  {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete'
		dateCompleted: '01/27/22'
	})
});
	.then((response) => response.json())
	.then((json) => console.log(json))

// 12.0
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',
});

